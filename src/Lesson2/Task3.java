package Lesson2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input  the length of side A");
        double radius = sc.nextDouble();
        double circumference = 2 * Math.PI*radius;

        System.out.println("Circumference = "+circumference);
    }
}

