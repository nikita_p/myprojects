package Lesson2;

import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input  the length of side A");
        double sideA = sc.nextDouble();
        System.out.println("Input  the length of side B");
        double sideB = sc.nextDouble();
        System.out.println("Input  the length of side C");
        double sideC = sc.nextDouble();

        double p = (sideA+sideB+sideC)/2;
        double areaOfATriangle = Math.sqrt(
                p * (p - sideA) * (p - sideB) * (p - sideC)
        );

        System.out.println("Area of a triangle = "+areaOfATriangle);

    }
}
