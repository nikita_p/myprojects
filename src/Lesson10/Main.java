package Lesson10;

public class Main {
    public static void main(String[] args) {

        Triangle triangleOne = new Triangle(8.0, 5.6, 4.3);
        Triangle triangleTwo = new Triangle(6.3, 4.0, 8.2);

        System.out.println(triangleOne.toString() + "Triangle area = " + triangleOne.getArea() + ".");
        System.out.println(triangleTwo.toString() + "Triangle area = " + triangleTwo.getArea() + ".");
    }
}
