package GraduateWork;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class FileClassifier{

    /**
     * Метод для переноса файлов в главную директорию (поддиректории удаляются)
     * @param mainCatalog <code>File</code> главная директория
     * @autor Nikita P
     */
    public static void removeToMainFile(File mainCatalog) {
        int k=1;
        while (k>0) {
            k=0;
            File [] listFiles = mainCatalog.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    File [] listFilesSD = listFiles[i].listFiles();
                    k++;
                    for (int j = 0; j < listFilesSD.length; j++) {
                        filenameCheck(listFilesSD[j], mainCatalog);
                        listFilesSD[j].renameTo(new File(mainCatalog, listFilesSD[j].getName()));
                    }
                    listFiles[i].delete();
                }
            }
        }
    }

    /**
     * Метод для проверки имени директории (совпадения имени, наличие имени и расширения)
     * @param checkedFile <code>File</code> проверяемый файл
     * @param mainCatalog <code>File</code> главная директория
     * @return checkedFile <code>File</code> фаайл с валидным именем
     * @autor Nikita P
     */
    public static File filenameCheck(File checkedFile, File mainCatalog) {
        String fileName = checkedFile.getName();
        int k = 1;

        for (; k>0; ){
            try {
                k=0;
                if(checkedFile.isDirectory()) {
                    break;
                }

                File [] listFiles = mainCatalog.listFiles();
                for (int i = 0; i < listFiles.length; i++) {
                    if (listFiles[i].getName().equals(fileName)) {
                        throw new SameNamesException();
                    }
                }

                if (fileName.equals("")){
                throw new EmptinessInputException();
                }else {char[] a = fileName.toCharArray();
                    int sum = 0;
                    for (int i = 0; i < a.length; i++) {
                        if (a[i] == '.')
                            sum = sum + 1;
                    }
                    if (sum<1){
                    throw new NoValueExtensionException();
                }}

            }catch (SameNamesException e){
                fileName = String.valueOf(JOptionPane.showInputDialog("Имя " + fileName + " уже существует.Введите новое имя для файла \n" + checkedFile.getAbsolutePath()));
                k++;
            }catch (NoValueExtensionException e){
                fileName = String.valueOf(JOptionPane.showInputDialog("Имя " + fileName + " не имеет разширения.Укажите имя с разширением."));
                k++;
            }catch (EmptinessInputException e){
                fileName = String.valueOf(JOptionPane.showInputDialog("Имя для файла не задано.Намишите имя файла:"));
                k++;
            }
        }
        checkedFile.renameTo(new File(checkedFile.getParent()+"\\"+ fileName));
        return  checkedFile;
    }

    /**
     * Метод для присвоения параметра расширение (extension)
     * @param checkedFile <code>File</code> файл для присвоения
     * @return <code>String</code> параметра расширения (extension)
     * @autor Nikita P
     */
    public static String outputFileExtension(File checkedFile) {
        String extension = null;
        String[] extensionArray = checkedFile.getName().split("\\.");
        if (extensionArray.length==1){
            extension = "NO EXTENSION";
        }
        if (extensionArray.length>1){
            for (int i = 1; i < extensionArray.length; i++){
                extension =  extensionArray[i];
            }
        }
    return extension;
    }

    /**
     * Метод для вычисления типа файла
     * @param checkedFile <code>File</code> вычисляемый файл
     * @return <code>FileType</code> тип файла
     * @autor Nikita P
     */
    public static FileType fileTypeDetection(File checkedFile) {
        Map<String, FileType> typeMap = new HashMap<>();
        typeMap.put("doc", FileType.DOCUMENT);
        typeMap.put("txt", FileType.DOCUMENT);
        typeMap.put("rtf", FileType.DOCUMENT);
        typeMap.put("docx", FileType.DOCUMENT);
        typeMap.put("odt", FileType.DOCUMENT);
        typeMap.put("xlx", FileType.DOCUMENT);
        typeMap.put("bmp", FileType.IMAGES);
        typeMap.put("tiff", FileType.IMAGES);
        typeMap.put("jpeg", FileType.IMAGES);
        typeMap.put("mp4", FileType.MOVIE);
        typeMap.put("avi", FileType.MOVIE);
        FileType type = typeMap.get(outputFileExtension(checkedFile));
        if (type==null){
            type=FileType.OTHER_FILES;
        }

        return type;
    }

    /**
     * Метод для вычисления типа размера файла
     * @param checkedFile <code>File</code> вычисляемый файл
     * @return <code>FileSize</code> тип размера файла
     * @autor Nikita P
     */
    public static FileSize fileSizeDetection(File checkedFile) {
        FileSize size=null;
        if (checkedFile.length()>=1073741824){
            size=FileSize.LARGE;
        }
        if(checkedFile.length()<1073741824 && checkedFile.length()>=1024){
            size=FileSize.MEDIUM;
        }
        if(checkedFile.length()<1024){
            size=FileSize.SMALL;
        }
        return size;
    }

    /**
     * Метод для создания масива MyFile []
     * @param mainCatalog <code>File</code> главная директория
     * @return <code>MyFile []</code> масив MyFile []
     * @autor Nikita P
     */
    public static MyFile [] myFileCreation(File mainCatalog) {
        File [] listFiles = mainCatalog.listFiles();
        MyFile [] myFiles = new MyFile [listFiles.length];
        for (int i = 0; i < listFiles.length; i++) {
            myFiles[i] = new MyFile (listFiles[i],
                    fileTypeDetection(listFiles[i]),
                    fileSizeDetection(listFiles[i]),
                    outputFileExtension(listFiles[i]));
        }
        return myFiles;
    }

    /**
     * Метод для создания директорий согласно разширению файлов вмасива MyFile []
     * @param myFiles <code>MyFile []</code> масив MyFile []
     * @param mainCatalog <code>File</code> главная директория
     * @return <code>String []</code> масив с уникальными именами директорий
     * @autor Nikita P
     */
    public static String [] creatingExtensionDirectories(MyFile [] myFiles,File mainCatalog) {
        String [] extensions = new String [myFiles.length];
        for (int i = 0; i < myFiles.length; i++) {
            extensions[i]=myFiles[i].getExtension();
            if (extensions[i] != null){
                File dir = new File(mainCatalog.getAbsoluteFile() + "\\" + extensions[i].toUpperCase());
                dir.mkdirs();
            }
        }return extensions;
    }

    /**
     * Метод для переноса файлов в соответствующую директорию согласно разширению
     * @param myFiles <code>MyFile []</code> масив MyFile []
     * @param mainCatalog <code>File</code> главная директория
     * @autor Nikita P
     */
    public static void sortByFileExtension(MyFile [] myFiles,File mainCatalog) {
        String [] directories = creatingExtensionDirectories (myFiles, mainCatalog);
        for (int i = 0; i < myFiles.length; i++) {
            for (int j = 0; j < directories.length; j++) {
                if (myFiles[i].getExtension().equals(directories[j])){
                    myFiles[i].getFile().renameTo(new File(mainCatalog.getAbsoluteFile()+"\\"+
                            directories[j].toUpperCase()+"\\"+
                            myFiles[i].getFile().getName()));
                }
            }
        }
    }

    /**
     * Метод для создания директорий согласно типу файлов в масиве MyFile []
     * @param myFiles <code>MyFile []</code> масив MyFile []
     * @param mainCatalog <code>File</code> главная директория
     * @return <code>String []</code> масив с уникальными именами директорий
     * @autor Nikita P
     */
    public static String [] creatingTypeDirectories(MyFile [] myFiles,File mainCatalog) {
        String [] type = new String [myFiles.length];
        for (int i = 0; i < myFiles.length; i++) {
                type[i]=myFiles[i].getType().toString();
            if (type[i] != null){
                File dir = new File(mainCatalog.getAbsoluteFile() + "\\" + type[i].toUpperCase());
                dir.mkdirs();
            }
        }return type;
    }

    /**
     * Метод для переноса файлов в соответствующую директорию согласно типа
     * @param myFiles <code>MyFile []</code> масив MyFile []
     * @param mainCatalog <code>File</code> главная директория
     * @autor Nikita P
     *
     */
    public static void sortByType(MyFile [] myFiles,File mainCatalog) {
        String [] directories = creatingTypeDirectories (myFiles, mainCatalog);
        for (int i = 0; i < myFiles.length; i++) {
            for (int j = 0; j < directories.length; j++) {
                if (myFiles[i].getType().toString().equals(directories[j])){
                    myFiles[i].getFile().renameTo(new File(mainCatalog.getAbsoluteFile()+"\\"+
                            directories[j].toUpperCase()+"\\"+
                            myFiles[i].getFile().getName()));
                }
            }
        }
    }

    /**
     * Метод для создания директорий согласно размера файлов вмасиве MyFile []
     * @param myFiles <code>MyFile []</code> масив MyFile []
     * @param mainCatalog <code>File</code> главная директория
     * @return <code>String []</code> масив с уникальными именами директорий
     * @autor Nikita P
     */
    public static String [] creatingSizeDirectories(MyFile [] myFiles,File mainCatalog) {
        String [] size = new String [myFiles.length];
        for (int i = 0; i < myFiles.length; i++) {
            size[i] = myFiles[i].getSize().toString();
            if (size[i] != null){
                File dir = new File(mainCatalog.getAbsoluteFile() + "\\" + size[i].toUpperCase());
                dir.mkdirs();
            }
        }return size;
    }

    /**
     * Метод для переноса файлов в соответствующую директорию согласно размера
     * @param myFiles <code>MyFile []</code> масив MyFile []
     * @param mainCatalog <code>File</code> главная директория
     * @autor Nikita P
     */
    public static void sortBySize(MyFile [] myFiles,File mainCatalog) {
        String [] directories = creatingSizeDirectories(myFiles, mainCatalog);
        for (int i = 0; i < myFiles.length; i++) {
            for (int j = 0; j < directories.length; j++) {
                if (myFiles[i].getSize().toString().equals(directories[j])){
                    myFiles[i].getFile().renameTo(new File(mainCatalog.getAbsoluteFile()+"\\"+
                            directories[j].toUpperCase()+"\\"+
                            myFiles[i].getFile().getName()));
                }
            }
        }
    }

    /**
     * Метод проверки файла на то, что он директория по абсолютному пути
     * @return <code>File []</code> директория
     * @autor Nikita P
     */
    public static File validToDirectory (){
        File catalog = null;
        String absolutePath;
        for (; ; ) {
            try {
                absolutePath = String.valueOf(JOptionPane.showInputDialog("Введите абсолютный путь к директории:"));
                catalog = new File(absolutePath);
                if (catalog.isDirectory()) {
                    break;
                } 
                if(absolutePath.equals("null")){
                    throw new NullPointerException();
                }
                if(absolutePath.equals("")){
                    throw new NoValueExtensionException();
                }
            }catch (NullPointerException e){
                break;
            }catch (NoValueExtensionException e){
                JOptionPane.showMessageDialog(null,"Нет значения");
            }
        }return catalog;
    }

    /**
     * Метод для создания файла с списком файлов
     * @param mainCatalog <code>File</code> главная директория
     * @param getAbsoluteFile <code>String</code> Абсолютный путь к изначальной главной директории
     * @autor Nikita P
     */
    public static void fileList(File mainCatalog, String getAbsoluteFile) {
        File [] listFiles = mainCatalog.listFiles();
        File files = new File(System.getProperty("user.dir") +
                "/src/GraduateWork/files.txt");
        try(PrintWriter pw = new PrintWriter(files)){
            for (int i = 0; i < listFiles.length; i++) {
                if (!listFiles[i].isDirectory()){
//                    MyFile f = new MyFile (listFiles[i],
//                            fileTypeDetection(listFiles[i]),
//                            fileSizeDetection(listFiles[i]),
//                            outputFileExtension(listFiles[i]));
//                    pw.println(f);
                    pw.println("        " + listFiles[i].getName());
                }
            }
            for (int j = 0; j < listFiles.length; j++) {
                if (listFiles[j].isDirectory()){
                    pw.println(listFiles[j].getAbsoluteFile().toString().replace(getAbsoluteFile,
                     ""));
                    fileList(listFiles[j],getAbsoluteFile);
                    pw.print(getStringFromFile(files));
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для вычитки текста из файла
     * @param files <code>File</code> файл для вычитки
     * @return <code>String</code> текст из файла
     * @autor Nikita P
     */
    public static String getStringFromFile(File files) {
        String text = "";
        try (Scanner sc = new Scanner(files)) {
            for (; sc.hasNextLine(); ) {
                text = text + sc.nextLine() + System.lineSeparator();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    /**
     * Метод для вывода списка файлов из текстового файла
     * @param text <code>String</code> текст из файла
     * @autor Nikita P
     */
    public static void conclusionStringFromFile(String text) {
        JOptionPane.showMessageDialog(null,"Список файлов для сортировки: \n \n" + text);
    }


}





