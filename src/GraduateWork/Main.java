package GraduateWork;

import javax.swing.*;
import java.io.File;

import static GraduateWork.FileClassifier.*;


public class Main {
    public static void main(String[] args) {
        File mainCatalog = validToDirectory();
        if(!mainCatalog.isDirectory()){
        }else{
            String main;
            for (; ; ){
                try {
                    main = String.valueOf(JOptionPane.showInputDialog("Выберете способ сортировки:" +
                            "\n e - классификация по расширению файлов (extension)" +
                            "\n t - классификация по типу файлов (type)" +
                            "\n s - классификация  по размеру файла (size)" +
                            "\n k - посмотреть список файлов в каталоге" +
                            "\n с - изменить каталог сортировки" +
                            "\n end - Exit"));
                    if (main.equals("e") || main.equals("t") || main.equals("s")  || main.equals("k") || main.equals("c")|| main.equals("end")){
                        if (main.equals("e")){
                            FileClassifier.removeToMainFile(mainCatalog);
                            MyFile [] myFiles = myFileCreation(mainCatalog);
                            sortByFileExtension(myFiles, mainCatalog);
                        }
                        if (main.equals("t")){
                            FileClassifier.removeToMainFile(mainCatalog);
                            MyFile [] myFiles = myFileCreation(mainCatalog);
                            sortByType(myFiles, mainCatalog);
                        }
                        if (main.equals("s")){
                            FileClassifier.removeToMainFile(mainCatalog);
                            MyFile [] myFiles = myFileCreation(mainCatalog);
                            sortBySize(myFiles, mainCatalog);
                        }
                        if (main.equals("k")){
                            fileList(mainCatalog,mainCatalog.getAbsolutePath());
                            File files = new File(System.getProperty("user.dir") +
                                    "/src/GraduateWork/files.txt");
                            conclusionStringFromFile(getStringFromFile(files));
                            files.delete();
                        }
                        if (main.equals("c")){
                            mainCatalog = validToDirectory();
                        }
                        if (main.equals("end")){
                            break;
                        }
                    }
                    if(main.equals("null")){
                        throw new NullPointerException();
                    }
                    if(main.equals("")){
                        throw new NoValueExtensionException();
                    }
                }catch (NullPointerException e){
                    break;
                }catch (NoValueExtensionException e){
                    JOptionPane.showMessageDialog(null,"Нет значения");
                }
            }
        }
    }
}


