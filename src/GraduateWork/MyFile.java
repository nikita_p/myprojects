package GraduateWork;


import java.io.File;

public class MyFile {
    public File file;
    private FileType type;
    private FileSize size;
    private String extension;

    public MyFile() {
    }

    public MyFile(File file, FileType type, FileSize size, String extension) {
        this.file = file;
        this.type = type;
        this.size = size;
        this.extension = extension;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public FileSize getSize() {
        return size;
    }

    public void setSize(FileSize size) {
        this.size = size;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }


    @Override
    public String toString() {
        return "{" +
                "name: '" + file.getName() + '\'' +
                ",   type: " + type +
                ",   size: " + size +
                ",   extension: '" + extension + '\'' +
                '}';
    }


}
