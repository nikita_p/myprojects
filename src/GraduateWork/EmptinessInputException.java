package GraduateWork;

public class EmptinessInputException extends Exception {

    public EmptinessInputException() {
        super();
    }

    public EmptinessInputException(String message) {
        super(message);
    }
}
