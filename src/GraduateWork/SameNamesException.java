package GraduateWork;

public class SameNamesException extends Exception {

    public SameNamesException() {
        super();
    }

    public SameNamesException(String message) {
        super(message);
    }
}
