package GraduateWork;

public class NoValueExtensionException extends Exception {

    public NoValueExtensionException() {
        super();
    }

    public NoValueExtensionException(String message) {
        super(message);
    }
}
