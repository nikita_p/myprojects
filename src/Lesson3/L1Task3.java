package Lesson3;


import java.util.Scanner;

public class L1Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input year");
        int year = sc.nextInt();

        if ((year % 4) == 0 && (year % 100) != 0 || (year % 4) == 0 && (year % 400) == 0){
            System.out.printf("This is a leap year.The number of days in a year - 366.");
        } else {
            System.out.printf("This is not a leap year.The number of days in a year - 365.");
        }
    }
}
