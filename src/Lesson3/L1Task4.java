package Lesson3;

import java.util.Scanner;

public class L1Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input  the length of side A");
        double sideA = sc.nextDouble();
        System.out.println("Input  the length of side B");
        double sideB = sc.nextDouble();
        System.out.println("Input  the length of side C");
        double sideC = sc.nextDouble();

        if (sideA + sideB > sideC && sideB + sideC > sideA && sideA + sideC > sideB ){
        System.out.println("The triangle exists");
        } else {
            System.out.println("The triangle does not exist");
        }

    }
}
