package Lesson3;

import java.util.Scanner;

public class L2Task4 {

    public static void main(String [] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Input 6-digit number");
        int number = sc.nextInt();
        int a = number/100000;
        int b = number%100000/10000;
        int c = number%10000/1000;
        int d = number%1000/100;
        int e = number%100/10;
        int f = number%10;

        if (a == f && b == e && c == d) {
            System.out.println("This number is a palindrome");
        }
        else {
            System.out.println("This number is not a palindrome");
        }
    }
}
