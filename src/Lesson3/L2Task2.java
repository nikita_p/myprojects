package Lesson3;

import java.util.Scanner;

public class L2Task2 {

    public static void main(String [] args) {
        Scanner sc = new Scanner(System.in);

        int xA = 0;
        int yA = 0;
        int xB = 4;
        int yB = 4;
        int xC = 6;
        int yC = 1;

        System.out.println("Input x");
        int x = sc.nextInt();
        System.out.println("Input  y");
        int y = sc.nextInt();

        int vectorAB = (xA - x) * (yB - yA) - (xB - xA) * (yA - y);
        int vectorAC = (xB - x) * (yC - yB) - (xC - xB) * (yB - y);
        int vectorBC = (xC - x) * (yA - yC) - (xA - xC) * (yC - y);

        if ((vectorAB >= 0) && (vectorAC >= 0) && (vectorBC >= 0) ||
                (vectorAB <= 0) && (vectorAC <= 0) && (vectorBC <= 0)) {
            System.out.println("This point lies inside the triangle");
        } else {
            System.out.println("This point does not lie inside the triangle");
        }
    }
}
