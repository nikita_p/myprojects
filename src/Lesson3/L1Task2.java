package Lesson3;

import java.util.Scanner;

public class L1Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input apartment number");
        int apartmentNumber = sc.nextInt();

        if (apartmentNumber > 0 && apartmentNumber <= 4 * 9 * 4) {
            int entrance = (apartmentNumber - 1) / 36 + 1;
            apartmentNumber = apartmentNumber - 36 * (entrance - 1);
            int floor = (apartmentNumber - 1) / 4 + 1;
            System.out.println("The apartment is located in the " + entrance + " entrance on the "+ floor  +" floor.");
        } else {
            System.out.println("Such there is no apartment in this house.");
        }
    }
}
