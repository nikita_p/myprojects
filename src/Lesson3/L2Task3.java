package Lesson3;

import java.util.Scanner;

public class L2Task3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input 4-digit number");
        int number = sc.nextInt();
        int a = number/1000;
        int b = number%1000/100;
        int c = number%100/10;
        int d = number%10;

        if (a + b == c + d) {
            System.out.println("This is a lucky ticket");
        } else {
            System.out.println("This is not a lucky ticket");
        }
    }
}
