package Lesson13;

public class Student extends People {
    private int recordBookNumber;

    public Student(String name, String surname,int age, Sex sex, int recordBookNumber) {
        super(name, surname, age, sex);
        this.recordBookNumber = recordBookNumber;
    }

    public int getRecordBookNumber() {
        return recordBookNumber;
    }

    public void setRecordBookNumber(int recordBookNumber) {
        this.recordBookNumber = recordBookNumber;
    }

    @Override
    public String toString() {
        return "Student {" +
                " name='" + super.getName() +
                "', surname='" + super.getSurname() +
                "', age='" + super.getAge() +
                "', sex='" + super.getSex() +
                "', RB Number='" + recordBookNumber +
                "'}";
    }

}
