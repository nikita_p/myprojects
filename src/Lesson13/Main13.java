package Lesson13;


import javax.swing.*;

public class Main13 {
    public static void main(String[] args){
        Group myGroup = new Group();
        String main;
        for (; ; ){
            try {
                main = String.valueOf(JOptionPane.showInputDialog("Input the command:" +
                        "\n add - add student to group" +
                        "\n del - delete student" +
                        "\n group - withdrawal of group students" +
                        "\n sort - sort student" +
                        "\n src - search student" +
                        "\n voen - show the list of recruit" +
                        "\n end - Exit"));
                if (main.equals("add") || main.equals("del") || main.equals("group") || main.equals("sort")|| main.equals("voen") || main.equals("src") || main.equals("end")){

                    if (main.equals("add")){
                        myGroup.addStudent();
                    }
                    if (main.equals("del")){
                        myGroup.deleteStudent();
                    }
                    if (main.equals("group")){
                        myGroup.withdrawalOfGroupStudents();
                    }
                    if (main.equals("sort")){
                        myGroup.sort();
                    }
                    if (main.equals("src")){
                        myGroup.searchStudent();
                    }
                    if (main.equals("voen")){
                        myGroup.getRecrut();
                    }
                    if (main.equals("end")){
                        break;
                    }
                }
            }catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,"No value");
                break;
            }
        }
    }
}

