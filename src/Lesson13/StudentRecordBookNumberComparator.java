package Lesson13;

import java.util.Comparator;

public class StudentRecordBookNumberComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Student student1 = (Student) o1;
        Student student2 = (Student) o2;

        if (student1.getRecordBookNumber() > student2.getRecordBookNumber()) {
            return 1;
        }
        if (student1.getRecordBookNumber() < student2.getRecordBookNumber()) {
            return -1;
        }
        return 0;
    }
}
