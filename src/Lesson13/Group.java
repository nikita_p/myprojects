package Lesson13;

import javax.swing.*;
import java.util.Arrays;
import java.util.Comparator;


public class Group implements Voenkom {

    private Student[] students = new Student[10];

    public String validToString (String value) {
        String st;
        for (; ; ){
            try {
                st = String.valueOf(JOptionPane.showInputDialog(value));
                if(st.length()>0 && st != null){
                    break;
                }
                if(st.equals("")){
                    throw new NullPointerException();}
                if (st==null) {
                    throw new MyException();
                }
            } catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,"No value");
            }catch (MyException e){
                JOptionPane.showMessageDialog(null,"No value specified or error number format.");
            }
        }return st;
    }

    public int validToNumber (String value) {
        int number = 0;
        for (; ; ){
            try {
                number = Integer.valueOf(JOptionPane.showInputDialog(value));
                if(number >0){
                    break;}
                if(number < 0){
                    throw new NullPointerException();
                }
                if(number == 0){
                    throw new NumberFormatException();
                }
            }catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null,"No value specified or error number format.");
            } catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,"Cancel");
                break;
            }
        }
        return number;
    }

    public int validToAge () {
        int age = 0;
        for (; ; ){
            try {
                age = Integer.valueOf(JOptionPane.showInputDialog("Enter student age:"));
                if(age >0 && age <=100){
                    break;}
                if(age < 0){
                    throw new NullPointerException();
                }
                if(age == 0){
                    throw new NumberFormatException();
                }
                if(age >100){
                    throw new MyException();
                }
            }catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null,"No value specified or error number format.");
            } catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,"Cancel");
                break;
            }catch (MyException e){
                JOptionPane.showMessageDialog(null,"The person did not pass the age test.");
            }
        }
        return age;
    }

    public Sex validToSex () {
        String sex="";
        Sex studSex = null;
        for (; ; ){
            try {
                sex = String.valueOf(JOptionPane.showInputDialog("Enter student record book number.\nm - MAN \nw - WOMAN"));

                if(sex.equals("m")||sex.equals("w")){
                    if(sex.equals("m")){
                        studSex = Sex.MAN;
                    }
                    if(sex.equals("w")){
                        studSex = Sex.WOMAN;
                    }break;
                }
                if (sex.equals("")){
                    throw new NumberFormatException();
                }
            }catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null,"No value specified or error number format.");
            } catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,"Cancel.");
                break;
            }
        }
        return studSex;
    }

    public Student createStudent () {
        String name = validToString ("Input student name:");
        String surname = validToString ("Enter student surname:");;
        int age = validToAge();
        int recordBookNumber = validToNumber("Enter student record book number:");
        Sex studSex = validToSex();
        Student st = new Student(name, surname, age, studSex ,recordBookNumber);
        return st;
    }



    public void addStudent () {
        int numberOfStudents = validToNumber("How many students do you want to add?");
        for (int n = 0; n < numberOfStudents; n++){
            Student student = createStudent ();
            boolean j = false;
            try {
                for (int i = 0; i < students.length; i++) {
                    if (students[i] == null) {
                    students[i] = student;
                    JOptionPane.showMessageDialog(null,"The student is included in the group. \n" + students[i]);
                        j=true;
                        break;
                }
            }

                try {
                    if(j==false){
                        throw new MyException();
                    }

                } catch (MyException e) {
                System.out.println("The " + student.toString() + " cannot be included in the group. The group is overcrowded.");
                }
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null,"Error number format");
            } catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,"Cancel");
                break;
            }
        }
    }

    private void sortGroupListAfterDelStudent(){
        for(int i = 0; i < students.length-1; i++){
            if(students[i] == null && students[i+1] != null){
                students[i] = students[i+1];
                students[i+1] = null;
            }
        }
    }

    public void deleteStudent() {
        int recordBookNumber = validToNumber("Enter the book number to delete the student:");

        try{

            for (int i = 0; i < students.length; i++) {

                if (students[i].getRecordBookNumber() == recordBookNumber && students[i] != null) {
                    students[i] = null;
                    sortGroupListAfterDelStudent();
                    JOptionPane.showMessageDialog(null,"Student with a record book number - "
                            + recordBookNumber
                            + " excluded");
                    return;
                }
            }
        }catch (NullPointerException e){
            JOptionPane.showMessageDialog(null,"There is no student in the group with a record book number - " + recordBookNumber);
        }
   }

    public void searchStudent() {
        String surname = String.valueOf(JOptionPane.showInputDialog("Input student surname:"));
        Student[] stud = new Student[students.length];
        int j=0;
        for (int i = 0; i < students.length; i++) {
            if (students[i] != null && students[i].getSurname().equals(surname)) {
                stud[j] = students[i];
                j++;
            }
        }
        JOptionPane.showMessageDialog(null, stud );
    }


    public void sortByName() {
        Arrays.sort(students, Comparator.nullsFirst( new StudentNameComparator()));
        for (int i = 0; i <students.length ; i++) {
            if(students[i] != null){
            }
        }
        JOptionPane.showMessageDialog(null, students );
    }

    public void sortBySurname() {
        Arrays.sort(students, Comparator.nullsFirst( new StudentSurnameComparator()));
        for (int i = 0; i <students.length ; i++) {
            if(students[i] != null){
            }
        }
        JOptionPane.showMessageDialog(null, students );
    }

    public void sortByAge() {
        Arrays.sort(students, Comparator.nullsFirst( new StudentAgeComparator()));
        for (int i = 0; i <students.length ; i++) {
            if(students[i] != null){
            }
        }
        JOptionPane.showMessageDialog(null, students );
    }

    public void sortBySex() {
        Arrays.sort(students, Comparator.nullsFirst( new StudentSexComparator()));
        for (int i = 0; i <students.length ; i++) {
            if(students[i] != null){
            }
        }
        JOptionPane.showMessageDialog(null, students );
    }
    public void sortByRecordBookNumber() {
        Arrays.sort(students, Comparator.nullsFirst( new StudentRecordBookNumberComparator()));
        for (int i = 0; i <students.length ; i++) {
            if(students[i] != null){
            }
        }
        JOptionPane.showMessageDialog(null, students );
    }


    public void withdrawalOfGroupStudents() {
        int k=0;
        for (int i = 0; i < students.length; i++) {
            if (students[i] != null) {
                k++;
            }
        }
        if(k==0){
            JOptionPane.showMessageDialog(null,"While the group is empty");
        }
        if(k>0){
            JOptionPane.showMessageDialog(null,students);
        }

    }


    public void sort() {
        String sortBy;
        for (; ; ){
            try {
                sortBy = String.valueOf(JOptionPane.showInputDialog("Sort by:" +
                        "\n n - Name" +
                        "\n s - Surname" +
                        "\n a - Age" +
                        "\n p - Sex" +
                        "\n r - Record Book Number"));
                if (sortBy.equals("n") || sortBy.equals("s") ||sortBy.equals("a") ||sortBy.equals("p") ||sortBy.equals("r")){

                    if (sortBy.equals("n")){
                        sortByName();
                    }
                    if (sortBy.equals("s")){
                        sortBySurname();
                    }
                    if (sortBy.equals("a")){
                        sortByAge();
                    }
                    if (sortBy.equals("p")){
                        sortBySex();
                    }
                    if (sortBy.equals("r")){
                        sortByRecordBookNumber();
                    }
                    break;
                } else{
                    throw new MyException();
                }
            }catch (MyException e){
                JOptionPane.showMessageDialog(null,"Incorrect sort field entered");
            }catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,"Cancel");
                break;
            }
        }
    }

    @Override
    public Student[] getRecrut() {
        Student[] studentValidForArmy = new Student[students.length];
        int j=0;
        for (int i = 0; i < students.length; i++) {
            if (students[i] != null && students[i].getSex()==Sex.MAN && students[i].getAge() >= 18) {
                studentValidForArmy[j] = students[i];
                j++;
            }
        }
        JOptionPane.showMessageDialog(null, studentValidForArmy );
        return studentValidForArmy;
    }

}


