package Lesson6;

import java.util.Arrays;
import java.util.Scanner;

public class L1Tasks {

    public static void main(String[] args) {

        Scanner sc1 = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        int [] arr = new int [15];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random()*100);
        }
        System.out.println("Array:");
        System.out.println(Arrays.toString(arr));

        System.out.println("");
        System.out.println("Task №1");
        int a = 5;
        System.out.println(max(arr));

        System.out.println("");
        System.out.println("Task №2");
        System.out.println(concat(a, 5.8, "Hello world "));

        System.out.println("");
        System.out.println("Task №3");
        rectangle(a, 6);

        System.out.println("");
        System.out.println("Task №4");
        System.out.println("Input number you want to find: ");
        int number = sc1.nextInt();
        int indexOfNumber = indexOfElement(arr, number);
        System.out.println("number index :" + indexOfNumber);

        System.out.println("");
        System.out.println("Task №5");
        System.out.println("Input text: ");
        String text = sc2.nextLine();
        System.out.println("There are " + wordCalc(text) + " words in a sentence");

    }

    /**
     * Calculate the maximum number in the array.
     *
     * @param arr <code>int[]</code> array
     * @return <code>int</code> max. Мaximum number.
     * @autor Nikita P
     */
    public static int max (int [] arr){
        int max = arr[0];
        for (int j : arr) {
            if (j > max) {
                max = j;
            }
        }
        return max;
    }

    /**
     * Concatenation of elements
     *
     * @param a <code>int</code> integer
     * @param b <code>double</code> real number
     * @param c <code>String</code> string
     * @retur <code>String</code> string
     * @autor Nikita P
     */
    public static String concat(int a, double b, String c) {
        return c + (a + b);
    }

    /**
     * Rectangle drawing method
     *
     * @param length <code>int</code> integer
     * @param width <code>int</code> integer
     * @autor Nikita P
     */
    public static void rectangle(int length, int width){
        for(int i=1;i<=width;i++){
            for(int j=1;j<=length;j++){
                System.out.print("*  ");
            }
            System.out.println();
        }
    }

    /**
     * Output item index
     *
     * @param arr  <code>int[]</code> array
     * @param number <code>int</code> integer
     * @return <code>int</code> index. Index of element.
     */
    static int indexOfElement(int[] arr, int number) {
        int index =0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == number){
                index = i;
                break;
            }
            else
                index = -1;
        }
        return index;
    }

    /**
     * Counting the number of words
     *
     * @param text <code>String</code> string
     * @return <code>int</code> words.length. Word count.
     */
    private static int wordCalc(String text) {
        text = text.trim();
        String[] words = text.split(" ");
        return words.length;
    }
}
