package Lesson6;

public class L2Task2 {
    public static void main(String[] args) {
        int [] arr;
        int a = 0;
        int b = 0;
        arr = palindromMax(a,b);
        System.out.println("The largest palindrome from multiplying numbers " + arr[1] + " and " + arr[2] + " = " + arr[0]);
    }

    /**
     * Palindrome check
     *
     * @param number <code>int</code> integer
     * @return result <code>boolean</code> boolean
     * @autor Nikita P
     */
    public static boolean palindromeTest(int number) {
        boolean result = false;
        String str = Integer.toString(number);
        if (str.length() == 6) {
            int num1 = number/100000;
            int num2 = number%100000/10000;
            int num3 = number%10000/1000;
            int num4 = number%1000/100;
            int num5 = number%100/10;
            int num6 = number%10;
            if (num1 == num6 && num2 == num5 && num3 == num4){
            result =true;
            } else
            result =false;
        }
        if (str.length() == 5) {
            int num1 = number%10000;
            int num2 = number%10000/1000;
            int num4 = number%100/10;
            int num5 = number%10;
            if (num1 == num5 && num2 == num4){
                result =true;
            } else
                result =false;
        }
        return result;
        }


    /**
     * Selection of the maximum palindrome
     *
     * @param a <code>int</code> integer
     * @param b <code>int</code> integer
     * @return arr <code>int[]</code> array
     * @autor Nikita P
     */
    public static int [] palindromMax(int a, int b) {
        int palindromMax = 0;
        for (int i = 100; i < 1000; i++) {
            for (int j = 100; j < 1000; j++) {
                if (palindromeTest(i * j) && (palindromMax < i * j)) {
                    palindromMax = i * j;
                    a = j;
                    b = i;
                }
            }

        }
        int [] arr = new int [3];
        arr [0] = palindromMax;
        arr [1] = a;
        arr [2] = b;
        return arr;
    }

}

