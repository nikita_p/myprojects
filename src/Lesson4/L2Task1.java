package Lesson4;

import java.util.Scanner;

public class L2Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int h;
        System.out.println("Input height");
        h = sc.nextInt();

        int b = 1;
        for (int i = 1; i <= h; ++i) {
            System.out.print("*");
            if (b <= i && b < h) {
                System.out.println();
                b++;
                i = 0;
            }
            if (h==i){
                System.out.println();
                h--;
                i = 0;
            }

        }
    }
}
