package Lesson4;

import java.util.Scanner;

public class L1Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int b;
        int h;
        System.out.println("Input width");
        b = sc.nextInt();
        System.out.println("Input height");
        h = sc.nextInt();

        for (int i = 1; i <= h; i++) {
            for (int j = 1; j <= b; j++) {
                if (j != 1 && i != 1 && j != b && i != h) {
                    System.out.print(" ");
                } else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }

    }
}

