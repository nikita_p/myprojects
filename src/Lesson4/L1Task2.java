package Lesson4;

import java.util.Scanner;

public class L1Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number;

        System.out.println("Input number");
        number = sc.nextInt();
        int result = number;
        if (number > 4 && number < 16) {
            for (int i = number-1; i >= 1; i--) {
                result = result * i;

            }
            System.out.println(result);
        }else {
            System.out.println("Wrong number");
        }

    }
}


