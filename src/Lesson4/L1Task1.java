package Lesson4;

import java.util.Scanner;

public class L1Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int line;
        System.out.println("Input number of stripes");
        line = sc.nextInt();

        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= line; j++) {
                if (j%2!=0) {
                    System.out.print("***");
                } else {
                    System.out.print("+++");
                }
            }
            System.out.println();
        }
    }
}
