package Lesson4;

public class Main4a {
    public static void main(String[] args) {
        for (int i = 1; i <= 20; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("?");
            }
            if (i % 3 == 0 && i % 5 != 0) {
                System.out.println(":)");
            }
            if (i % 5 == 0 && i % 3 != 0) {
                System.out.println(":(");
            }
            if (i % 3 != 0 && i % 5 != 0) {
                System.out.println(i);
            }
        }
    }
}

