package Lesson4;

import java.util.Scanner;

public class L2Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int b;
        System.out.println("Input width (odd number) ");
        b = sc.nextInt();

        if (b % 2 != 0) {
            for (int i = 1; i <= b; i++) {
                for (int j = 1; j <= b; j++) {
                    if (i <= j & i <= (b + 1) - j | i >= j & i >= (b + 1) - j) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                    if (j % b == 0) {
                        System.out.println("");
                    }
                }
            }
        }else {
            System.out.println("Wrong width");
        }
    }
}
