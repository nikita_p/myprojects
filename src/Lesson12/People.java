package Lesson12;

public class People {
    private String name;
    private String surname;

    public People(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public String toString() {
        return "People{" +
                "name='" + name +
                ", surname='" + surname +
                '}';
    }

}
