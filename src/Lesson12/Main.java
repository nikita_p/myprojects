package Lesson12;


public class Main {
    public static void main(String[] args) throws ArrayOutOfBoundsException {
        Group myGroup = new Group();
        Student Vova = new Student("Vova", "A.", 1001);
        Student Sveta = new Student("Sveta", "B.", 1002);
        Student Sasha = new Student("Sasha", "C.", 1003);
        Student Alena = new Student("Alena", "D.", 1004);
        Student Lena = new Student("Lena", "E.", 1005);
        Student Kostya = new Student("Kostya", "F.", 1006);
        Student Yana = new Student("Yana", "G.", 1007);
        Student Katya = new Student("Katya", "H.", 1008);
        Student Sergei = new Student("Sergei", "I.", 1009);
        Student Alexei = new Student("Alexei", "J.", 1010);
        Student Ira = new Student("Ira", "K.", 1011);
        Student Vlad = new Student("Vlad", "L.", 1012);
        myGroup.addStudent(Vova);
        myGroup.addStudent(Sveta);
        myGroup.addStudent(Sasha);
        myGroup.addStudent(Alena);
        myGroup.addStudent(Lena);
        myGroup.addStudent(Kostya);
        myGroup.addStudent(Yana);
        myGroup.addStudent(Katya);
        myGroup.addStudent(Sergei);
        myGroup.addStudent(Alexei);
        myGroup.addStudent(Ira);
        myGroup.addStudent(Vlad);
        myGroup.deleteStudent(1010);
        myGroup.deleteStudent(1016);
        myGroup.searchStudent("F.");
        System.out.println(myGroup.searchStudent("G."));
        myGroup.searchStudent("N.");



    }
}
