package Lesson12;

public class Student extends People{
    private int recordBookNumber;

    public Student(String name, String surname, int recordBookNumber) {
        super(name, surname);
        this.recordBookNumber = recordBookNumber;
    }

    public int getRecordBookNumber() {
        return recordBookNumber;
    }

    public void setRecordBookNumber(int recordBookNumber) {
        this.recordBookNumber = recordBookNumber;
    }

    @Override
    public String toString() {
        return "Student {" +
                " name='" + super.getName() +
                "', surname='" + super.getSurname() +
                "', record Book Number='" + recordBookNumber +
                "'}";
    }

}
