package Lesson12;

public class ArrayOutOfBoundsException extends Exception {
    public ArrayOutOfBoundsException() {
        super();
    }

    public ArrayOutOfBoundsException(String message) {
        super(message);
    }
}
