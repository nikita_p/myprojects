package Lesson12;


public class Group {

    private Student[] students = new Student[10];



    public void addStudent (Student student) {

        boolean j = false;
        for (int i = 0; i < students.length; i++) {
            if (students[i] == null) {
                students[i] = student;
                System.out.println("The student "
                        + students[i].getName() + " "
                        + students[i].getSurname()
                        + " is included in the group.");
                return;

            }
        }

            try {
                if(j==false){
                    throw new ArrayOutOfBoundsException();
                }

            } catch (ArrayOutOfBoundsException e) {
                System.out.println("The " + student.toString() + " cannot be included in the group. The group is overcrowded.");

        }
    }

    public void deleteStudent(int recordBookNumber) {
        try{
            int k=1;
            for (int i = 0; i < students.length; i++) {
                k++;
                if (students[i].getRecordBookNumber() == recordBookNumber) {
                    students[i] = null;
                    System.out.println("Student with a record book number - "
                            + recordBookNumber
                            + " excluded");
                    return;
                }
                if(k==students.length) {
                    System.out.println("There is no student in the group with a record book number - " + recordBookNumber);
                }
            }
        }catch (NullPointerException e){

        }
   }

    public Student searchStudent(String surname) {
        try{
            int k=1;
            for (int i = 0; i < students.length; i++) {
                k++;
                if (students[i].getSurname() == surname) {
                    System.out.println("Student "+surname+" found. " + students[i].toString());
                    return students[i];
                }
                if(k==students.length) {
                    System.out.println("Student '"+ surname +"' not found.");
                }
            }
        }catch (NullPointerException e){

        }
        return null;
    }

}


