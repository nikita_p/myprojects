package Lesson5;

import java.util.Arrays;

public class L2Task2 {
    public static void main(String[] args) {
        int[] arr1 = {7, 2, 9, 4};
        System.out.println(Arrays.toString(arr1));


        int n = arr1.length;
        for (int i = 0; i < n / 2; i++) {
            int k = arr1[i];
            arr1[i] = arr1[n - 1 - i];
            arr1[n - 1 - i] = k;
        }

        System.out.print(Arrays.toString(arr1));
    }
}