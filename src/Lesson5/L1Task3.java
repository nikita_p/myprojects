package Lesson5;

import java.util.Arrays;

public class L1Task3 {

    public static void main(String[] args) {

        int[] array = new int[15];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int)(Math.random()*10);
        }

        int[] doubleArray = Arrays.copyOf(array, 2*array.length);
        for (int j = 0; j < doubleArray.length - array.length; j++) {
            doubleArray[array.length + j] = array[j] * 2;
        }

        System.out.println("Было → " + Arrays.toString(array));
        System.out.println("Стало → " + Arrays.toString(doubleArray));
    }
}

