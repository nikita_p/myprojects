package Lesson5;

import java.util.Scanner;

public class L2Task3 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter your amount of money");
        String number = sc.nextLine();

        String [] b = number.split("[.,]");
        int dollars = Integer.parseInt(b[0]);

        int cents = 0;
        if (b.length>1){
            cents = Integer.parseInt(b[1]);
        }


        if (dollars+cents>999999999.99) {
            System.out.println("Don't lie to yourself =)");
        }

        else {
            String numbers1 = ",one,two,three,four,fife,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen";
            String [] block1 = numbers1.split("[,]");
            String numbers2 = ",,twenty,thirty,forty,fifty,sixty,seventy,eighty,ninety";
            String [] block2 = numbers2.split("[,]");
            String numbers3 = ",hundred,thousand,million";
            String [] block3 = numbers3.split("[,]");

            System.out.print("You have ");
            if (dollars/100000000!=0){
                System.out.print(block1[dollars/100000000]+" "+block3[1]+" ");
            }
            if (dollars%100000000>9999999){
                System.out.print(block2[dollars%100000000/10000000]+" ");
                if (dollars>19999999){
                    System.out.print(block1[dollars%10000000/1000000]+" ");
                }
            }
            if ((dollars%100000000/1000000<20)&&(dollars>99999999)){
                System.out.print(block1[dollars%100000000/1000000]+" ");
            }
            if (dollars/1000000<20){
                System.out.print(block1[dollars/1000000]+" ");
            }
            if (dollars>999999){
                System.out.print("million ");
            }

            if (dollars%1000000/100000!=0){
                System.out.print(block1[dollars%1000000/100000]+" "+block3[1]+" ");
            }
            if ((dollars%100000/1000<10)&&(dollars>100000)&&(dollars<1000000)){
                System.out.print(block1[dollars%100000/1000]+" "+block3[2]+" ");
            }
            if ((dollars%100000/10000!=0)&&(dollars/1000>20)){
                System.out.print(block2[dollars%100000/10000]+" ");

                if ((dollars%10000/1000!=0)){
                    System.out.print(block1[dollars%10000/1000]+" ");
                }
                if ((dollars%100000/1000<20)){
                    System.out.print(block1[dollars%100000/1000]+" ");
                }
                System.out.print("thousand ");
            }
            if ((dollars%100000==0)&&(dollars/1000>20)&&(dollars<1000000)){
                System.out.print("thousand ");
            }
            if ((dollars>999)&&(dollars/1000<20)){
                System.out.print(block1[dollars/1000]+" "+block3[2]+" ");
            }

            if (dollars%1000/100!=0){
                System.out.print(block1[dollars%1000/100]+" "+block3[1]+" ");
            }
            if ((dollars>=20)&&(dollars%1000%100!=0)){
                System.out.print(block2[dollars%100/10]+" ");

                if ((dollars%100>20)){
                    System.out.print(block1[dollars%10]+" ");
                }
            }
            if ((dollars>20)&&(dollars%100<20)){
                System.out.print(block1[dollars%100]+" ");
            }
            if (dollars<20){
                System.out.print(block1[dollars]+" ");
            }
            if (dollars>1){
                System.out.print("dollars"+" ");
            }
            else if (dollars==0){
                System.out.print("no dollars"+" ");
            }
            else {
                System.out.print("dollar"+" ");
            }


            if (cents>0){

                String e = "";

                if (b.length>1) {
                    e = b[1].substring(0,1);
                }


                if (e.equals("0")){
                    System.out.print(block1[cents]+" ");
                }
                if ((b[1].length()==1)&&(cents<10)){
                    cents*=10;
                    if(cents>=10){
                    System.out.print(block2[cents/10]+" ");
                    cents/=10;
                    }
                    if (cents==10){
                        System.out.print(block1[cents]+" ");
                        cents/=10;
                    }
                }
                if ((cents>10)&&(cents<20)){
                    System.out.print(block1[cents]+" ");
                }
                if ((cents>=20)&&(cents<100)&&(cents%10==0)){
                    System.out.print(block2[cents/10]+" ");
                }
                if ((cents>20)&&(cents<100)&&(cents%10!=0)){
                    System.out.print(block2[cents/10]+" ");
                    System.out.print(block1[cents%10]+" ");
                }
                System.out.print("cents");
            }

        }


    }

}

