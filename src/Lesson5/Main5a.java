package Lesson5;

import java.util.Arrays;

public class Main5a {

    public static void main(String[] args) {

        int[] pay = new int[12];

        for (int i = 0; i < pay.length; i++) {
            pay[i] = (int)(10_000 + Math.random()*10_000);
        }
        System.out.println(Arrays.toString(pay));

        int sum = 0;

        for(int element:pay){
            sum = sum + element;
        }

        System.out.println(sum/pay.length);
    }
}