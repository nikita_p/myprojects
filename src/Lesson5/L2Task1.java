package Lesson5;

import java.util.Arrays;

public class L2Task1 {
    public static void main(String[] args) {
        int[][] a = new int[6][6];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = j + 1;
            }
        }

        System.out.println("It was:");
        for(int[] row :a){
            System.out.println(Arrays.toString(row));
        }
        int b;

        for (int i = 0; i < a.length / 2; i++) {
            for (int j = 0; j < a.length / 2; j++) {
                b = a[i][j];
                a[i][j] = a[a.length - 1 - j][i];
                a[a.length - 1 - j][i] = a[a.length - 1 - i][a.length - 1 - j];
                a[a.length - 1 - i][a.length - 1 - j] = a[j][a.length - 1 - i];
                a[j][a.length - 1 - i] = b;
            }
        }


        System.out.println();
        System.out.println("Inverted 90 degrees:");
        for(int[] row :a){
            System.out.println(Arrays.toString(row));
        }


        for (int i = 0; i < a.length / 2; i++) {
            for (int j = 0; j < a.length / 2; j++) {
                b = a[i][j];
                a[i][j] = a[a.length - 1 - j][i];
                a[a.length - 1 - j][i] = a[a.length - 1 - i][a.length - 1 - j];
                a[a.length - 1 - i][a.length - 1 - j] = a[j][a.length - 1 - i];
                a[j][a.length - 1 - i] = b;
            }
        }


        System.out.println();
        System.out.println("Inverted 180 degrees:");
        for(int[] row :a){
            System.out.println(Arrays.toString(row));
        }


        for (int i = 0; i < a.length / 2; i++) {
            for (int j = 0; j < a.length / 2; j++) {
                b = a[i][j];
                a[i][j] = a[a.length - 1 - j][i];
                a[a.length - 1 - j][i] = a[a.length - 1 - i][a.length - 1 - j];
                a[a.length - 1 - i][a.length - 1 - j] = a[j][a.length - 1 - i];
                a[j][a.length - 1 - i] = b;
            } }


        System.out.println();
        System.out.println("Inverted 270 degrees:");
        for(int[] row :a){
            System.out.println(Arrays.toString(row));
        }
    }
}