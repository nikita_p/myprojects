package Lesson5;

import java.util.Scanner;

public class L1Task4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input text");
        String text = sc.nextLine();

        char[] a = text.toCharArray();
        int sum = 0;

        for (int i = 0; i < a.length; i++) {
            if (a[i] == 'b')
                sum = sum + 1;
        }

        System.out.println("'b' in text: " + sum);
    }
}
