package Lesson5;

import java.util.Arrays;
import java.util.Scanner;

public class L1Task2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Input the size of the array:");
        int size = sc.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            System.out.print("Input the value with the index " + i +":");
            int value = sc.nextInt();
            array[i] = value;
        }

        System.out.println(Arrays.toString(array));
    }
}


