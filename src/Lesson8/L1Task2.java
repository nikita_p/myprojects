package Lesson8;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class L1Task2 {

    public static void main(String[] args) throws FileNotFoundException {
        File f1 = new File("1.txt");
        try(PrintWriter pw = new PrintWriter(f1)){
            pw.println(text(arr1()));
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    /**
     * Create a 2D Array
     *
     * @return arr1 <code>int[]</code> array
     * @autor Nikita P
     */
    public static int[][] arr1(){
        int[][] arr1 = new int[6][8];
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[0].length; j++) {
                arr1[i][j] = (int)(Math.random()*100);
            }
        }
        return arr1;
    }

    /**
     * Array translation in string
     *
     * @param array <code>int[]</code> array
     * @return text <code>String</code> string
     * @autor Nikita P
     */
    public static String text(int[][] array) {
        String text = "";
        for(int i = 0; i < array.length; i++){
            for(int j = 0;j < array[i].length;j++){
                text = text + arr1()[i][j] + " ";
            }
            text = text + "\n";
        }
        return text;
    }

}

