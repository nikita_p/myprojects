package Lesson8;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class L1Task1 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Input name of your file: ");
        String name = sc.nextLine();

        File myFile = new File(name + ".txt");

        System.out.println("Input text of your file" +
                "\nTo exit press \"q\" on a new line.");
        try (PrintWriter pw = new PrintWriter(myFile)){
            myFile.createNewFile();


            String line = sc.nextLine();

            while (!line.equals("q")){
                pw.println(line);
                line = sc.nextLine();
            }
            System.out.println("File "  + name + ".txt saved");

        } catch (IOException e) {
            System.out.println("Error creation file!");
            e.printStackTrace();
        }
    }
}


