package Lesson8;

import java.io.File;

public class L1Task3 {
    public static void main(String[] args) {
        String catalog = ".";
        catalogs(catalog);
    }

    /**
     * Directory listing
     *
     * @param catalog <code>String</code> string
     *@autor Nikita P
     */
    static void catalogs(String catalog) {
        File file = new File(catalog);
        if (file.isDirectory()) {
            String[] array = file.list();
            for (int i = 0; i < array.length; i++) {
                File f = new File(catalog + "\\" + array[i]);
                if (f.isDirectory())
                    System.out.println(array[i]);
            }
        }
    }
}

