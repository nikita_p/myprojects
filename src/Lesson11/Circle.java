package Lesson11;

public class Circle extends Shape {
    private Point p1;
    private Point p2;

    public Circle() {
        super();
    }

    public Circle(Point p1, Point p2) {
        super();
        this.p1 = p1;
        this.p2 = p2;
    }

    public Point getP1() {
        return p1;
    }

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public Point getP2() {
        return p2;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }

    /**
     * Calculating the perimeter of a circle
     *
     * @return <code>double</code> p. Circle perimeter.
     * @autor Nikita P
     */
    @Override
    double getPerimeter() {
        double p = 2 * Math.PI * Point.getLength(p1, p2);
        return p;
    }

    /**
     * Calculating the area of a circle
     *
     * @return <code>double</code> a. Circle are.
     * @autor Nikita P
     */
    @Override
    double getArea() {
        double a = Math.PI * Math.pow(Point.getLength(p1, p2), 2);
        return a;
    }

    @Override
    public String toString() {
        return "Circle [p1 = " + p1 + ", p2 = " + p2 + "]";
    }

}
