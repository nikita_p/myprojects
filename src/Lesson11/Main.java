package Lesson11;

public class Main {

    public static void main(String[] args) {
        Point p1 = new Point(5.3, 8);
        Point p2 = new Point(7.7, 1);
        Point p3 = new Point(4, 3.4);
        Circle c1 = new Circle(p1, p2);
        Shape s1 = c1;
        System.out.println(s1);
        System.out.println("Perimeter = " + s1.getPerimeter());
        System.out.println("Area = " + s1.getArea());
        Triangle t1 = new Triangle(p1, p2, p3);
        Shape s2 = t1;
        System.out.println(s2);
        System.out.println("Perimeter = " + s2.getPerimeter());
        System.out.println("Area = " + s2.getArea());
    }

}