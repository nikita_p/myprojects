package Lesson11;

public abstract class Shape {

    abstract double getPerimeter();
    abstract double getArea();

}
