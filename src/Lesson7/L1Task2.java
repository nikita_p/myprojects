package Lesson7;



public class L1Task2 {
    public static void main(String[] args) {

        int[] arr = new int[10];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random()*100);
        }

        System.out.println(arraysToString(arr));
    }

    /**
     * Arrays.toString () method for int [].
     *
     * @param array <code>int[]</code> array
     * @retur k <code>String</code> string
     * @autor Nikita P
     */
    public static String arraysToString(int[] array) {
        String k = "";
        for(int i = 0; i < array.length; i++){
            k = k + array[i] + " ";
        }
        return k;
    }

}

