package Lesson7;

import java.util.Scanner;

public class L1Task3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Input your number in binary to convert to decimal: ");
        String binary = scanner.nextLine();

        if (conversionToDecimal(binary) > 0) {
            System.out.println("Entered number in binary: " + binary);
            System.out.println("Decimal number: " + conversionToDecimal(binary));
        }
    }


    /**
     * Checking and converting to decimal format
     *
     * @param str <code>String</code> string
     * @return sum <code>int</code> integer
     * @autor Nikita P
     */
    static  int conversionToDecimal(String str){
        int sum=0;
        int k;
        char a;
        for(int i=0;i<str.length();i++){
            a=str.charAt(str.length()-1-i);
            if(a=='1'| a=='0'){
                if (a=='1'){
                    k=1;
                }
                else{
                    k=0;
                }
            }
            else{
                System.out.println("error number");
                sum=0;
                break;
            }
            sum+=k*Math.pow(2, i);
        }
        return sum;
    }

}
