package Lesson7;

import java.util.Calendar;

public class L1Task1 {
    public static void main(String[] args) {

        Calendar cl1 = Calendar.getInstance();
        cl1.getTime();

        int year =cl1.get(Calendar.YEAR);
        int month = cl1.get(Calendar.MONTH);
        int day =cl1.get(Calendar.DAY_OF_MONTH );

        Calendar cl2 = Calendar.getInstance();
        cl2.set(year,month -1,day);

        long t1= cl1.getTimeInMillis();
        long t2 = cl2.getTimeInMillis();



        System.out.println(t1-t2);

    }
}
