package Lesson9;

public class Cat {
    protected String name;
    protected int age;
    protected double weight;
    protected String color;


    public Cat() {
    }

    public Cat(String name, int age, double weight, String color) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.color = color;
    }

    void vote(){
        System.out.println("Myav-Myav");
    }

    public void hooliganism (){
        String[] hooligan = new String[5];
        hooligan[0] = "throws things off";
        hooligan[1] = "scratches furniture";
        hooligan[2] = "wakes up";
        hooligan[3] = "attacks the feet";
        hooligan[4] = "hisses";
        System.out.println(name +" misbehaves - "+ hooligan[(int)(Math.random() * hooligan.length)]);
    }

    public void catEat (){
        System.out.println(name + " eating...");
    }

    public void print() {
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Weight: " + weight);
        System.out.println("Color: " + color);
    }


}